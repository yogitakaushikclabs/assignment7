package com.example.yogita.assignment7;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


public class MainActivity2 extends Activity {

    RelativeLayout layout;
   Button btn1;
    TextView tv;
    Button[] btn = new Button[6];
int currentCircle;
    int currentState1[]=new int[6];
    int currentCircle1[]={0,0,0,0,0,0};
    public static final int STATE_RED = R.drawable.circle1;
    public static final int STATE_YELLOW = R.drawable.yellow_circle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity2);
        layout = (RelativeLayout) findViewById(R.id.layout3);

        for (int i = 0; i < layout.getChildCount(); i++) {
            btn[i] = (Button) layout.getChildAt(i);
        }
        for(int i=0;i<btn.length;i++)
        {
            currentState1[i]=STATE_RED;
        }


        btn1 = (Button) findViewById(R.id.circlebtn);
        tv = (TextView) findViewById(R.id.textview1);
        layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int a = (int) event.getX();
                int b = (int) event.getY();
                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        circle(a, b);

                        break;

                    case MotionEvent.ACTION_MOVE:

                        circle(a, b);
                        break;

                    case MotionEvent.ACTION_UP:
                    for(int i=0;i<btn.length;i++)
                        currentCircle1[i]=0;
                        break;
                    default:
                        break;
                }

                return true;
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity2, menu);
        return true;
    }


    public void circle(int x, int y) {

        int[] centerX = new int[6];
        int[] centerY = new int[6];
        int k;

        int radius = (int)btn1.getWidth() / 2;

        int[] dist1 = new int[6];

        for (k = 0; k <btn.length; k++) {
            centerX[k] = (int) btn[k].getLeft() + radius;
            centerY[k] = (int) btn[k].getTop() + radius;
        }
        for (int j = 0; j < 6; j++) {
            dist1[j] = (int) Math.sqrt(Math.pow(x - centerX[j], 2) + Math.pow(y - centerY[j], 2));
        }
        for (int s = 0; s < 6; s++) {
            if (dist1[s] < radius) {

                if (currentCircle1[s] == 0) {
                    currentCircle1[s] = 1;
                    if (currentState1[s] == STATE_RED) {
                        currentState1[s] = STATE_YELLOW;
                        btn[s].setBackgroundResource(STATE_YELLOW);
                    } else {
                        currentState1[s] = STATE_RED;
                        btn[s].setBackgroundResource(STATE_RED);
                    }
                }

            }


        }


    }

}



