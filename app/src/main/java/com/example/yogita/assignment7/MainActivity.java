package com.example.yogita.assignment7;

import android.app.Activity;
import android.app.Notification;
import android.content.Intent;
import android.drm.DrmStore;
import android.graphics.drawable.Drawable;
import android.location.Location;
//import com.android.maps.GeoPoint;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import static android.widget.Toast.*;


public class MainActivity extends Activity {

    TextView tv;
    View layout;
    Button button;
    int currentState = STATE_RED;
    int currentCircle;
    public static final int STATE_RED = R.drawable.circle1;
    public static final int STATE_YELLOW = R.drawable.yellow_circle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.circlebtn);
        layout = (View) findViewById(R.id.layout3);
        tv = (TextView) findViewById(R.id.textview1);
        layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int a = (int) event.getX();
                int b = (int) event.getX();
                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:

                        circle(a, b);
                        tv.setText("X is" + event.getX() + "Y is" + event.getY());
                        break;

                    case MotionEvent.ACTION_MOVE:

                        circle(a, b);
                        break;

                    case MotionEvent.ACTION_UP:
                        currentCircle = 0;
                        break;
                    default:
                        break;
                }

                return false;
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
public void next(View v)
{
    Intent intent= new Intent(this,MainActivity2.class);
    startActivity(intent);
}

    public void circle(int x, int y) {

        int center_y = layout.getHeight() / 2;
        int center_x = layout.getWidth() / 2;
        int radius = button.getWidth() / 2;
        int dist = (int) Math.sqrt(Math.pow(x - center_x, 2) + Math.pow(y - center_y, 2));


        if (dist < radius) {
            Toast.makeText(this, "inside the circle", Toast.LENGTH_LONG).show();
            if (currentCircle == 0) {
                currentCircle = 1;
                if (currentState == STATE_RED) {
                    currentState = STATE_YELLOW;
                    button.setBackgroundResource(STATE_YELLOW);
                } else {
                    currentState = STATE_RED;
                    button.setBackgroundResource(STATE_RED);
                }
            } else if (dist > radius) {
                currentCircle = 0;
                Toast.makeText(this, "outside the circle", Toast.LENGTH_LONG).show();
            } else {
                currentCircle = 0;
                Toast.makeText(this, "on the circle", Toast.LENGTH_LONG).show();
            }
        }
    }
}